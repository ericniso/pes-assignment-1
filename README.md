# Assignment 1

**Versione del progetto:** 0.0.2 (5/11/18)

**Componenti del gruppo:** Eric Nisoli #807147

**Aspetti di DevOps sviluppati:** Containerization, Continuous Integration/Continuous Development/Continuous Deployment

## Descrizione del progetto

Il progetto consinste in una semplice pagina web che permette ad un utente di 
effettuare l'upload di un file in formato [FASTA](https://en.wikipedia.org/wiki/FASTA_format)
e ottenere il calcolo del [Grafo di de Bruijn](https://en.wikipedia.org/wiki/De_Bruijn_graph)
corrispondente.  
Il servizio è composto da cinque diversi componenti:
*   **Nginx**: le richieste sono processate attraverso un server web http
    che si comporta da proxy, effettuando il forward di queste ultime al modulo
    corrispondente in base al tipo di URL richiesto; nel caso in cui l'URL 
    iniziasse con il prefisso `/api`, la richiesta sarà lasciata in gestione al 
    modulo di **API** che ha il compito di gestire l'esecuzione delle operazioni 
    di creazione del grafo, in tutti gli altri casi la richiesta verrà presa in 
    carico dal modulo **Static** che si occupa di servire i file statici quali 
    pagine html e file javascript e css.

*   **API**: le api per la creazione del grafo sono rese disponibili da un web 
    server scritto utilizzando il framework Node.js. Il compito di questo modulo 
    consinste nel verificare la presenza di un grafo già calcolato (attraverso 
    il calcolo di un md5 checksum del file FASTA fornito in input) verificando 
    la presenza del grafo all'interno di un server di **Cache**. In caso affermativo, 
    questo modulo restituirà direttamente come risultato il grafo già calcolato in 
    precedenza e salvato,
    poichè il calcolo di un grafo di de Bruijn è un'operazione deterministica che 
    a parità di input restituirà sempre il medesimo output; si presta 
    dunque bene al caching dato che spesso il calcolo di questi grafi è un'operazione 
    onerosa che può richiedere dai pochi secondi a diversi minuti a seconda della 
    grandezza dell'input, quindi utilizzando un hash md5 per effettuare il caching 
    delle richieste, è possibile ridurre sensibilmente il tempo di attesa delle risposte http 
    effettuate utilizzando il medesimo file di input. 
    Nel caso in cui il grafo non si trovi nel server di 
    cache, questo modulo demanda l'operazione di calcolo del risultato al modulo 
    di **Compute**, e una volta ottenuto il risultato si occuperà di salvarlo 
    all'interno del server di cache e  restituirlo all'utente che ne ha fatto 
    richiesta.

*   **Static**: è stato deciso di implementare un modulo con l'unico scopo di 
    servire file statici attraverso il protocollo HTTP poichè nel mondo odierno 
    è pratica comune aggiornare frequentemente le interfacce grafiche e lo stile 
    delle pagine web. A causa di ciò, ogni update dell'ambiente grafico del 
    progetto (pagine html, javascript e css) avrebbe causato un disservizio 
    delle funzionalità di **API** nel caso in cui fossero rimaste coese 
    all'interno dello stesso modulo, poichè un aggiornamento significa 
    un downtime del servizio, e sebbene possa essere solamente di qualche secondo,
    potrebbe causare il fallimente di operazioni complesse di calcolo di un 
    grafo di de Bruijn in esecuzione in quel momento.

*   **Compute**: questo modulo riceve una richiesta http con il nome del file 
    di input tramite un server web scritto in Node.js, ed 
    utilizza tale file per il calcolo del grafo di de Bruijn tramite 
    un algoritmo scritto in C, restituendo il risultato al client che ha 
    effettuato la richiesta.

*   **Cache**: il caching è gestito da un dbms Postgresql. È stato deciso di 
    utilizzare un dbms relazionale a causa della presenza di solamente una tabella 
    composta da tre attributi (md5 hash, numero di kmer e grafo di de Bruijn) 
    facilmente indirizzabile tramite indexing della chiave primaria composta da 
    hash e kmer.

## Utilizzo degli strumenti di DevOps

* **Containerization**: il servizio fornito dall'applicativo è logicamente 
    suddiviso in diversi moduli singoli che collaborano tra di loro. È stato 
    scelto di implementare una soluzione di containerizzazione di questi moduli 
    tramite Docker a causa della enorme diffusione avuto da questa piattaforma 
    e dal supporto che riceve quotidianamente dagli sviluppatori e dalla community. 
    Inoltre molti servizi di hosting e virtualization offrono la gestione di 
    container docker out-of-the-box, offrendo anche soluzioni di maintainment di 
    cluster di servizi containerizzati. In particolare è stato scelto di utilizzare 
    un container per ogni modulo descritto in precedenza, sfruttando così l'isolamento 
    delle risorse software necessarie al loro funzionamento, ma anche utilizzando 
    le reti logiche fornite da docker per la comunicazione intranet dei diversi 
    moduli che devono cooperare tra di loro.
    Oltre alle reti logiche stato deciso di utilizzare un volume per lo storage
    persistente dei dati all'interno del database e un ulteriore volume per 
    lo scambio di file tra servizi containerizzati, poichè il livello di 
    astrazione è talmente alto che permette di mettere in condivisione dei file 
    leggendo e scrivendo i dati in una cartella locale al container che sta 
    eseguendo il processo di lettura/scrittura.
    È stato utilizzato `docker-compose` per facilitare la dichiarazione di 
    link tra container e la creazione di reti virtuali e volumi condivisi.

    Di seguito uno schema riassuntivo 
    della rete di interazione dei moduli.

![service containers](images/net.png)

*   **CI/CD**:
    è stato scelto di utilizzare la pipeline di CI/CD integrata con la piattaforma GitLab 
    in quanto il codice sorgente è hostato su questa piattaforma.
    La pipeline di CI/CD è stata suddivisa in tre diversi stage:

    *   **Build**: prevede un solo job, responsabile di verificare la 
        correttezza del building del progetto su una macchina remota.

    *   **Test**: prevede anch'esso solamente un job il quale ha il compito di 
        avviare il container contenente il codice del modulo API utilizzando 
        un diverso entrypoint che eseguirà dei test di integrazione per la 
        verifica della corretta collaborazione tra i servizi di API, Database e 
        Worker.

    *   **Deploy**: una volta superate le fasi precedenti, il job di questo stage
        si occuperà di aggiornare il codice dei servizi attivi in produzione.

    In particolare gli stage Build e Test sono eseguiti su una macchina remota, 
    mentre il Deploy è effettuato su una macchina remota differente, così da 
    concentrare le risorse computazionali sulle richieste effettuate da utenti 
    veri e propri.

    Il servizio è disponibile all'indirizzo [http://hague.niso.io](http://hague.niso.io)
