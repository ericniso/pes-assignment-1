'use strict';

const { Client } = require('pg')

exports.insert = async (hash, kmer, graph) => {

    let res = undefined;
    
    const client = new Client();
    await client.connect();
    
    try
    {
        res =
            await client.query('INSERT INTO hashes VALUES ($1::text, $2::integer, $3::text)', 
                [hash, kmer, graph]);
    } catch (err)
    {
        res = undefined;
    }
    finally
    {
        await client.end();
    }

    return res;
};

exports.check = async (hash, kmer) => {

    const client = new Client();
    await client.connect();

    const res = 
        await client.query('SELECT * FROM hashes WHERE md5=$1::text AND kmer=$2::integer', [hash, kmer]);
    await client.end();

    return res.rows[0];
};
