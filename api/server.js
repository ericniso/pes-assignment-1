'use strict';

//ciao 

const Hapi = require('hapi');
const API_SUFFIX = '/api';
const uuid = require('uuid/v1');
const FILES_DIR = process.env.FILES_DIR || '/tmp';
const db = require('./db');
const worker = require('./worker');
const utils = require('./utils');
const Readable = require('stream').Readable

const server = Hapi.server({
    port: process.env.HTTP_PORT || 3000
});

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

const init = async () => {

    await server.register(require('inert'));

    server.route({
        method: 'POST',
        path: `${API_SUFFIX}/compute/{kmer}`,
        config: {
            payload: {
                output: 'stream',
                parse: true,
                allow: 'multipart/form-data'
            }
        },
        handler: async (request, h) => {

            const kmer = request.params.kmer;
            const data = request.payload['file'];

            if (data)
            {
                const name = data.hapi.filename;
                const filename = uuid() + '.fa.gz';
                const path = FILES_DIR + '/' + filename;

                const file = await utils.saveFile(data, path);
                const hash = await utils.hash(path);
                const exists = await db.check(hash, kmer);

                if (!exists)
                {
                    const result = await worker.exec(filename, kmer);
                    const insert = await db.insert(hash, kmer, result.graph);
                    const stream = new Readable();
                    stream.push(result.graph);
                    stream.push(null);

                    return h.response(stream)
                        .type('text/csv')
                        .header('Content-type', 'text/csv')
                        .header('Content-length', stream.length)
                        .header('Content-Disposition: attachment; filename="graph.csv"');
                }
                else
                {
                    const stream = new Readable();
                    stream.push(exists.graph);

                    stream.push(null);

                    return h.response(stream)
                        .type('text/csv')
                        .header('Content-type', 'text/csv')
                        .header('Content-length', stream.length)
                        .header('Content-Disposition: attachment; filename="graph.csv"');
                }
            }
            else
            {
                return h.response(null).code(400);
            }
        }
    })

    await server.start();
    console.log(`Server running at: ${server.info.uri}`);
};

init();
