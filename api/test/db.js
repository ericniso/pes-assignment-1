'use strict';

const db = require('../db');
const assert = require('assert');

describe('DB', () => {

    it('should not exists',  async () => {
        const check = await db.check('my_hash', 3);
        assert(check === undefined);
        return true;
    });

    it('should be inserted',  async () => {
        const ins = await db.insert('my_hash', 3, 'my_graph');
        assert(ins !== undefined);
        return true;
    });

    it('should exists',  async () => {
        const check = await db.check('my_hash', 3);
        assert(check !== undefined);
        return true;
    });
    
});
