'use strict';

const Hapi = require('hapi');
const FILES_DIR = process.env.FILES_DIR || '/tmp';
const { spawn } = require('child-process-async');

const server = Hapi.server({
    port: process.env.HTTP_PORT || 3000
});


const init = async () => {

    await server.register(require('inert'));

    server.route({
        method: 'GET',
        path: '/compute/{filename}/{kmer}',
        handler: async function (request, h) {

            const filename = request.params.filename;
            const kmer = request.params.kmer;
            let result = '';
            
            const child = spawn('hague', ['-f', `${FILES_DIR}/${filename}`, '-k', kmer]);
            child.stdout.on('data', (data) => { result += data.toString('utf8'); });
            const { stdout, stderr, exitCode } = await child;

            return { code: exitCode, graph: result };
        }
    });

    await server.start();
    console.log(`Server running at: ${server.info.uri}`);
};

init();
